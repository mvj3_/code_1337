package com.xu81.tw4a;

import java.util.List;

import android.app.Activity;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class TiddlyWiki4Android extends Activity {

	private WebView mainWebView;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
                
                //webview对象
		mainWebView = (WebView) findViewById(R.id.mainWebView);

		WebSettings setting = mainWebView.getSettings();
		setting.setJavaScriptEnabled(true);//允许javascript
		mainWebView.setWebChromeClient(new WebChromeClient());//初始化WebChromeClient对象
                Bean b = new Bean();
                b.setTitle("title");
		mainWebView.addJavascriptInterface(b, "bean");//添加javascript对象
		mainWebView.loadUrl("file:///android_asset/test.html");//加载本地html页面
	}
}